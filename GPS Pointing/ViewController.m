//
//  ViewController.m
//  GPS Pointing
//
//  Created by Sven Gehring on 16.09.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "Communicator.h"
#import "GPSUtils.h"

@implementation ViewController

@synthesize lblLongitude, lblLatitude, lblAccuracy, lblHeading, lblVerticalAccuracy, lblAltitude, lblGyroX, lblGyroY, lblGyroZ, lblBearingLeft, lblBearingRight, ipAdress;
@synthesize gpsSwitch, currentHeading, currentLocation;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ipAdress.delegate = self;
    
    [[MyLocationManager sharedInstance] setDebugDelegate:self];
	
    // View loaded -> do initializations    
   /* [self.mapView.userLocation addObserver:self
                                forKeyPath:@"location" 
                                   options:(NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld) 
                                   context:nil];
    */
    // Set points for display
    // HOME
    /*leftLat = 49.269352;
    leftLong = 7.043612;
    rightLat = 49.269226;
    rightLong = 7.043663;*/
    
    //DFKI
    /*leftLat = 49.25810;
    leftLong =  7.04160;
    rightLat = 49.25803;
    rightLong = 7.04303;*/
    /*
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    leftLat = appDelegate.leftLat;
    leftLong = appDelegate.leftLon;
    rightLat = appDelegate.rightLat;
    rightLong = appDelegate.rightLon;
    
    midpoint = [GPSUtils midpoint:CLLocationCoordinate2DMake(leftLat, leftLong) secondPoint:CLLocationCoordinate2DMake(rightLat, rightLong)];
    
    currentPitch = 0.0;
    deltaHeight = appDelegate.lower - 1; // lower display boundary is the real lower display boundary - height position of the phone (1 meter)
    displayHeight = appDelegate.upper;
    
    NSLog(@"%.7f", 1000*[GPSUtils haversineDistance:CLLocationCoordinate2DMake(leftLat, leftLong) secondPoint:CLLocationCoordinate2DMake(rightLat, rightLong)]);
    
    CLLocationCoordinate2D coords[2] = {CLLocationCoordinate2DMake(leftLat, leftLong), CLLocationCoordinate2DMake(rightLat, rightLong)};
    displayLine = [MKPolyline polylineWithCoordinates:coords count:2];	
    [mapView addOverlay:displayLine];
    
    drawHitOverlay = NO;
    */
    // Load the user preferences
    [self updateFieldsFromSettings];
    
    // init the map view
    //[self initMapView];
    
    
    // Start tracking of device orientation
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    //[locManager stopUpdatingHeading];
    //[locManager stopUpdatingLocation];
    //[motionManager stopDeviceMotionUpdates];
    
    lblAccuracy.text = @"-";
    lblAltitude.text = @"-";
    lblHeading.text = @"-";
    lblLatitude.text = @"-";
    lblGyroX.text = @"-";
    lblGyroZ.text = @"-";
    lblLongitude.text = @"-";
    lblGyroY.text = @"-";
    lblVerticalAccuracy.text = @"-";
    
    /*[self.mapView.userLocation removeObserver:self forKeyPath:@"location"];
    [self.mapView removeFromSuperview]; // release crashes app
    self.mapView = nil;
*/
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)disconnect:(id)sender
{
    MyLocationManager* ml = [MyLocationManager sharedInstance];
 
    // stop the lovation services
    [ml.locManager stopUpdatingHeading];
    [ml.locManager stopUpdatingLocation];
    [ml.motionManager stopDeviceMotionUpdates];
    NSLog(@"stopped location manager...");
    
    [[Communicator sharedInstance] closeConnection];
}

- (IBAction)connect:(id)sender
{
    Communicator* c = [Communicator sharedInstance];
    [c setIpAdress:self.ipAdress.text];
    [c sendConnect];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // Any additional checks to ensure you have the correct textField here.
    [textField resignFirstResponder];
    
    return true;
}

-(void) updateDebug:(CLLocation*) curLocation bearLeft:(int) bl bearRight:(int) br pitch:(double)cp
{
    self.currentLocation = curLocation;
    
    lblBearingLeft.text = [NSString stringWithFormat:@"%i°", bl];
    lblBearingRight.text = [NSString stringWithFormat:@"%i°", br];
    
    [self updateLocationDisplays];
}

-(void) updateDebug:(CLLocationDirection)curHeading
{
    self.currentHeading = curHeading;
    [self updateHeadingDisplays];
}

#pragma mark
#pragma mark GUI Update Methods
/*
- (void)initMapView{
    [mapView setDelegate:self];
    
    // Mark the display sides on the map
    MKPointAnnotation *leftSide = [[MKPointAnnotation alloc] init];
    leftSide.coordinate = CLLocationCoordinate2DMake(leftLat, leftLong);
    [self.mapView addAnnotation:leftSide];
    
    MKPointAnnotation *rightSide = [[MKPointAnnotation alloc] init];
    rightSide.coordinate = CLLocationCoordinate2DMake(rightLat, rightLong);
    [self.mapView addAnnotation:rightSide];
    
    MKPointAnnotation *mid = [[MKPointAnnotation alloc] init];
    mid.coordinate = midpoint;
    [self.mapView addAnnotation:mid];
    
    MKPointAnnotation *current = [[MKPointAnnotation alloc] init];
    current.coordinate = self.currentLocation.coordinate;
    [self.mapView addAnnotation:current];
    
    // Show the current orientation of the user
    [mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading animated:YES];
}
*/
- (void)updateHeadingDisplays{
    lblHeading.text = [NSString stringWithFormat:@"%.2f", self.currentHeading];
    MyLocationManager* ml = [MyLocationManager sharedInstance];
    lblGyroX.text = [NSString stringWithFormat:@"%.6f", ml.gyroX];
    lblGyroY.text = [NSString stringWithFormat:@"%.6f", ml.gyroY];
    lblGyroZ.text = [NSString stringWithFormat:@"%.6f", ml.gyroZ];
}

- (void)updateLocationDisplays{
    lblLatitude.text = [NSString stringWithFormat:@"%.6f", self.currentLocation.coordinate.latitude];
    lblLongitude.text = [NSString stringWithFormat:@"%.6f", self.currentLocation.coordinate.longitude];
    lblAltitude.text = [NSString stringWithFormat:@"%.6f", self.currentLocation.altitude];
    lblAccuracy.text = [NSString stringWithFormat:@"%.6f", self.currentLocation.horizontalAccuracy];
    lblVerticalAccuracy.text = [NSString stringWithFormat:@"%.6f", self.currentLocation.verticalAccuracy];
}

- (void)updateFieldsFromSettings{
    
}


#pragma mark -
#pragma mark Display hit calculation
/*
- (BOOL)displayHit{
    
    
    BOOL isHit = NO;
    
    int angleDifference = bearingToRight - bearingToLeft;
    double y = 0;
    
    // if difference negative, span over 0°
    if (angleDifference < 0){
        if (((int)currentHeading >= bearingToLeft) || ((int)currentHeading <= bearingToRight)){
            // horitontally in display area
            double distance = [GPSUtils haversineDistance:self.currentLocation.coordinate secondPoint:self.midpoint];
            y = distance * 1000 * tan(currentPitch) - deltaHeight;
            if (y <= displayHeight && y >= 0) {
                //vertically in display area
                isHit = YES;
            }
        }
    } else{
        if (((int)currentHeading >= bearingToLeft) && ((int)currentHeading <= bearingToRight)){
            // horitontally in display area
            double distance = [GPSUtils haversineDistance:self.currentLocation.coordinate secondPoint:self.midpoint];
            y = distance * 1000 * tan(currentPitch) - deltaHeight;
            if (y <= displayHeight && y >= 0) {
                //vertically in display area
                isHit = YES;
            }
        }
    }
    
    if (isHit){
        // draw overlay GREEN
        if (drawHitOverlay){
            // Line already green, no need to repaint
        } else {
            drawHitOverlay = YES;
            [mapView removeOverlay:displayLine];
            [mapView addOverlay:displayLine];
        }
        
        //show hit point
        if (hitPoint != nil) {
            [self.mapView removeAnnotation:hitPoint]; // remove old one first!
        }
        hitPoint = [[MKPointAnnotation alloc] init];
        
        int brng1 = self.currentHeading;
        int brng2 = [GPSUtils bearing:CLLocationCoordinate2DMake(leftLat, leftLong) secondPoint:CLLocationCoordinate2DMake(rightLat, rightLong)];
        
        hitPoint.coordinate = [GPSUtils pathIntersection:currentLocation.coordinate bearing1:brng1 secondPoint:CLLocationCoordinate2DMake(leftLat, leftLong) bearing2:brng2];
        
        double x = [GPSUtils equirectangularDistance:CLLocationCoordinate2DMake(leftLat, leftLong) secondPoint:hitPoint.coordinate];
        NSLog(@"position on target: x : %f, y : %f in meter", x, y);
        
        [self.mapView addAnnotation:hitPoint];
    } else {
        // draw Overlay RED
        if (!drawHitOverlay){
            // Line already red, no need to repaint
        } else {
            drawHitOverlay = NO;
            [mapView removeOverlay:displayLine];
            [mapView addOverlay:displayLine];
        }
        if (hitPoint != nil) {
            [self.mapView removeAnnotation:hitPoint]; // remove old one first!
        }
    }
    
    
    if (isHit) { // display is hit on x-axis, check if also on y-axis
        double distance = [GPSUtils haversineDistance:self.currentLocation.coordinate secondPoint:self.midpoint];
        double y = distance * 1000 * tan(currentPitch) - deltaHeight;
        NSLog(@"y in m: %f", y);
    }
 
    
    return isHit;
}
*/
#pragma mark
#pragma mark Core Location Stuff
/*
- (void)startLocationEvents {
    if (!self.locManager) {
        CLLocationManager* theManager = [[CLLocationManager alloc] init];//[[[CLLocationManager alloc] init] autorelease];
        
        // Retain the object in a property.
        self.locManager = theManager;
        locManager.delegate = self;
    }
    
    // Start location services to get the true heading.
    locManager.distanceFilter = 1;
    locManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    [locManager startUpdatingLocation];
    
    // Start heading updates.
    if ([CLLocationManager headingAvailable]) {
        locManager.headingFilter = .1;
        [locManager startUpdatingHeading];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    self.currentLocation = newLocation;
    
    bearingToLeft = [GPSUtils bearing:self.currentLocation.coordinate secondPoint:CLLocationCoordinate2DMake(leftLat, leftLong)];
    bearingToRight = [GPSUtils bearing:self.currentLocation.coordinate secondPoint:CLLocationCoordinate2DMake(rightLat, rightLong)];
    
    lblBearingLeft.text = [NSString stringWithFormat:@"%i°", bearingToLeft];
    lblBearingRight.text = [NSString stringWithFormat:@"%i°", bearingToRight];
    
    [self updateLocationDisplays];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    if (newHeading.headingAccuracy < 0)
        return;
    
    // Use the true heading if it is valid.
    CLLocationDirection  theHeading = ((newHeading.trueHeading > 0) ?
                                       newHeading.trueHeading : newHeading.magneticHeading);
    
    self.currentHeading = theHeading;
    
    [self updateHeadingDisplays];
    
    // Check if horizontally pointing in display range
    [self displayHit];
    //NSLog(@"Horizontally pointing to display: %i", [self displayHit]);
    
    // Rotate the map according to the compass orientation
    //[mapView setTransform:CGAffineTransformMakeRotation(-1 * theHeading * 3.14159 / 180)];
}*/

#pragma mark
#pragma mark Map functions
/*
- (void)mapView:(MKMapView *)mv regionWillChangeAnimated:(BOOL)animated{
    
}

// Listen to change in the userLocation
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    MKCoordinateRegion region;
    region.center = self.mapView.userLocation.coordinate;
    
    MKCoordinateSpan span;
    span.latitudeDelta  = 0.001; // Change these values to change the zoom
    span.longitudeDelta = 0.001;
    region.span = span;
    
    [self.mapView setRegion:region animated:YES];
    
    // Show the current orientation of the user
    [mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading animated:YES];
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay{
    if ([overlay isKindOfClass:[MKPolyline class]])
    {
        displayLineView = [[MKPolylineView alloc] initWithPolyline:displayLine];
        if (drawHitOverlay){
            displayLineView.fillColor = [UIColor greenColor];
            displayLineView.strokeColor = [UIColor greenColor];
        }
        else {
            displayLineView.fillColor = [UIColor redColor];
            displayLineView.strokeColor = [UIColor redColor];
        }
        displayLineView.lineWidth = 5;
        
        return displayLineView;
    }
    
    return nil;
}
*/
@end
