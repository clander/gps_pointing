//
//  Communicator.h
//  GPS Pointing
//
//  Created by Christian Lander on 17.01.13.
//
//

#import <Foundation/Foundation.h>
#import "GCDAsyncUdpSocket.h"

@protocol ImageDelegate <NSObject>

-(void) updateImage:(UIImage*) img;

@end

@interface Communicator : NSObject<GCDAsyncUdpSocketDelegate> {
    
    long _tag;
}

@property (nonatomic, retain) GCDAsyncUdpSocket* udpSocket;
@property (nonatomic, retain) NSString* ipAdress;
@property (nonatomic, retain) id<ImageDelegate> imgDelegate;

+ (id)sharedInstance;
- (id)init;
-(void) sendConnect;
-(void) closeConnection;
-(void) sendUpdate:(float)x andY:(float)y;
- (NSString *)getIPAddress;

@end
