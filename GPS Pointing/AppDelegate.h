//
//  AppDelegate.h
//  GPS Pointing
//
//  Created by Sven Gehring on 16.09.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Globals.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    
    // Get user preference
    NSUserDefaults *defaults;
    
    /*float leftLat;
     float leftLon;
     float rightLat;
     float rightLon;
     float lower;
     float upper;
     */
}

@property (strong, nonatomic) UIWindow *window;
@property float leftLat;
@property float leftLon;
@property float rightLat;
@property float rightLon;
@property float lower;
@property float upper;


@end
