//
//  InteractionViewController.m
//  GPS Pointing
//
//  Created by Christian Lander on 12.12.12.
//
//

#import "InteractionViewController.h"
#import "MyLocationManager.h"

@interface InteractionViewController ()

@end

@implementation InteractionViewController

@synthesize image, mapView, displayLine, hitPoint, displayLineView, midpoint, currentHeading, currentLocation, udpSocket;

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // View loaded -> do initializations
    [self.mapView.userLocation addObserver:self
                                forKeyPath:@"location"
                                   options:(NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld)
                                   context:nil];
    
    // Set points for display
    // HOME
    /*leftLat = 49.269352;
     leftLong = 7.043612;
     rightLat = 49.269226;
     rightLong = 7.043663;*/
    
    //DFKI
    /*leftLat = 49.25810;
     leftLong =  7.04160;
     rightLat = 49.25803;
     rightLong = 7.04303;*/
    
    
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    leftLat = appDelegate.leftLat;
    leftLong = appDelegate.leftLon;
    rightLat = appDelegate.rightLat;
    rightLong = appDelegate.rightLon;
    
    currentPitch = 0.0;
    deltaHeight = appDelegate.lower - 1; // lower display boundary is the real lower display boundary - height position of the phone (1 meter)
    displayHeight = appDelegate.upper;
    
    midpoint = [GPSUtils midpoint:CLLocationCoordinate2DMake(appDelegate.leftLat, appDelegate.leftLon) secondPoint:CLLocationCoordinate2DMake(appDelegate.rightLat, appDelegate.rightLon)];
    
    CLLocationCoordinate2D coords[2] = {CLLocationCoordinate2DMake(leftLat, leftLong), CLLocationCoordinate2DMake(rightLat, rightLong)};
    displayLine = [MKPolyline polylineWithCoordinates:coords count:2];
    [mapView addOverlay:displayLine];
    
    drawHitOverlay = NO;
    
    [[MyLocationManager sharedInstance] setMapDelegate:self];
    
    // init the map view
    [self initMapView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initMapView{
    [mapView setDelegate:self];
    
    // Mark the display sides on the map
    MKPointAnnotation *leftSide = [[MKPointAnnotation alloc] init];
    leftSide.coordinate = CLLocationCoordinate2DMake(leftLat, leftLong);
    [self.mapView addAnnotation:leftSide];
    
    MKPointAnnotation *rightSide = [[MKPointAnnotation alloc] init];
    rightSide.coordinate = CLLocationCoordinate2DMake(rightLat, rightLong);
    [self.mapView addAnnotation:rightSide];
    
    MKPointAnnotation *mid = [[MKPointAnnotation alloc] init];
    mid.coordinate = midpoint;
    [self.mapView addAnnotation:mid];
    
    MKPointAnnotation *current = [[MKPointAnnotation alloc] init];
    current.coordinate = self.currentLocation.coordinate;
    [self.mapView addAnnotation:current];
    
    // Show the current orientation of the user
    [mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading animated:YES];
}

-(void) updateView:(NSData*) data
{
    
}

-(void) updateMap:(CLLocation*) curLocation bearLeft:(int) bl bearRight:(int) br heading:(CLLocationDirection) curHeading pitch:(double)cp
{
    self.currentLocation = curLocation;
    self.currentHeading = curHeading;
    currentPitch = cp;
    bearingToLeft = bl;
    bearingToRight = br;
    
    if ([self displayHit]) {
        //TODO send hitpoint
    }
}

#pragma mark -
#pragma mark Display hit calculation
- (BOOL)displayHit{
    
    /*
            360°/0°
     270°             90°
            180°
     */
    
    BOOL isHit = NO;
    
    int angleDifference = bearingToRight - bearingToLeft;
    double y = 0;
    
    // if difference negative, span over 0°
    if (angleDifference < 0){
        if (((int)currentHeading >= bearingToLeft) || ((int)currentHeading <= bearingToRight)){
            // horitontally in display area
            double distance = [GPSUtils haversineDistance:self.currentLocation.coordinate secondPoint:self.midpoint];
            y = distance * 1000 * tan(currentPitch) - deltaHeight;
            if (y <= displayHeight && y >= 0) {
                //vertically in display area
                isHit = YES;
            }
        }
    } else{
        if (((int)currentHeading >= bearingToLeft) && ((int)currentHeading <= bearingToRight)){
            // horitontally in display area
            double distance = [GPSUtils haversineDistance:self.currentLocation.coordinate secondPoint:self.midpoint];
            y = distance * 1000 * tan(currentPitch) - deltaHeight;
            if (y <= displayHeight && y >= 0) {
                //vertically in display area
                isHit = YES;
            }
        }
    }
    
    if (isHit){
        // draw overlay GREEN
        if (drawHitOverlay){
            // Line already green, no need to repaint
        } else {
            drawHitOverlay = YES;
            [mapView removeOverlay:displayLine];
            [mapView addOverlay:displayLine];
        }
        
        //show hit point
        if (hitPoint != nil) {
            [self.mapView removeAnnotation:hitPoint]; // remove old one first!
        }
        hitPoint = [[MKPointAnnotation alloc] init];
        
        int brng1 = self.currentHeading;
        int brng2 = [GPSUtils bearing:CLLocationCoordinate2DMake(leftLat, leftLong) secondPoint:CLLocationCoordinate2DMake(rightLat, rightLong)];
        
        hitPoint.coordinate = [GPSUtils pathIntersection:currentLocation.coordinate bearing1:brng1 secondPoint:CLLocationCoordinate2DMake(leftLat, leftLong) bearing2:brng2];
        
        double x = [GPSUtils equirectangularDistance:CLLocationCoordinate2DMake(leftLat, leftLong) secondPoint:hitPoint.coordinate];
        NSLog(@"position on target: x : %f, y : %f in meter", x, y);
        
        Communicator* c = [Communicator sharedInstance];
        c.imgDelegate = self;
        [c sendUpdate:x andY:y];
        
        [self.mapView addAnnotation:hitPoint];
    } else {
        // draw Overlay RED
        if (!drawHitOverlay){
            // Line already red, no need to repaint
        } else {
            drawHitOverlay = NO;
            [mapView removeOverlay:displayLine];
            [mapView addOverlay:displayLine];
        }
        if (hitPoint != nil) {
            [self.mapView removeAnnotation:hitPoint]; // remove old one first!
        }
    }
    
    /*
     if (isHit) { // display is hit on x-axis, check if also on y-axis
     double distance = [GPSUtils haversineDistance:self.currentLocation.coordinate secondPoint:self.midpoint];
     double y = distance * 1000 * tan(currentPitch) - deltaHeight;
     NSLog(@"y in m: %f", y);
     }
     */
    
    return isHit;
}

-(void) updateImage:(UIImage*) img
{
    [image setImage:img];
}

#pragma mark
#pragma mark Map functions

- (void)mapView:(MKMapView *)mv regionWillChangeAnimated:(BOOL)animated{
    
}

// Listen to change in the userLocation
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    MKCoordinateRegion region;
    region.center = self.mapView.userLocation.coordinate;
    
    MKCoordinateSpan span;
    span.latitudeDelta  = 0.001; // Change these values to change the zoom
    span.longitudeDelta = 0.001;
    region.span = span;
    
    [self.mapView setRegion:region animated:YES];
    
    // Show the current orientation of the user
    [mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading animated:YES];
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay{
    if ([overlay isKindOfClass:[MKPolyline class]])
    {
        displayLineView = [[MKPolylineView alloc] initWithPolyline:displayLine];
        if (drawHitOverlay){
            displayLineView.fillColor = [UIColor greenColor];
            displayLineView.strokeColor = [UIColor greenColor];
        }
        else {
            displayLineView.fillColor = [UIColor redColor];
            displayLineView.strokeColor = [UIColor redColor];
        }
        displayLineView.lineWidth = 5;
        
        return displayLineView;
    }
    
    return nil;
}

@end
