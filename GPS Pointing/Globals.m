//
//  Globals.m
//  GPS Pointing
//
//  Created by Christian Lander on 12.12.12.
//
//

#import "Globals.h"

@implementation Globals

NSString* const DISPLAY_LEFT_LONG = @"leftlong";
NSString* const DISPLAY_LEFT_LAT = @"leftlat";
NSString* const DISPLAY_RIGHT_LONG = @"rightlong";
NSString* const DISPLAY_RIGHT_LAT = @"rightlat";

NSString* const DISPLAY_LOWER_BOUND = @"lower";
NSString* const DISPLAY_UPPER_BOUND = @"upper";

int const PORT = 9876;

@end
