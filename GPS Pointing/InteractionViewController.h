//
//  InteractionViewController.h
//  GPS Pointing
//
//  Created by Christian Lander on 12.12.12.
//
//

#import <UIKit/UIKit.h>
#import "Communicator.h"
#import "ViewController.h"
#import <MapKit/MapKit.h>
#import "MyLocationManager.h"
#import "GPSUtils.h"
#import "AppDelegate.h"

@interface InteractionViewController : UIViewController <MapDelegate, MKMapViewDelegate, ImageDelegate> {

    IBOutlet UIImageView* image;
    IBOutlet MKMapView *mapView;
    
    MKPolyline *displayLine;
    MKPointAnnotation *hitPoint;
    MKPolylineView *displayLineView;
    
    CLLocationDirection currentHeading;
    CLLocation *currentLocation;
    CLLocationCoordinate2D midpoint;
    
    int bearingToLeft;
    int bearingToRight;
    
    float leftLat;
    float leftLong;
    float rightLat;
    float rightLong;
    
    double deltaHeight; // height difference between phone and lower display boundary
    double currentPitch; // in RADIANS
    
    double displayHeight;
    
    BOOL drawHitOverlay;
    
}



@property (nonatomic, retain) GCDAsyncUdpSocket* udpSocket;
@property (nonatomic, retain) IBOutlet UIImageView* image;
@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic, retain) MKPolyline *displayLine;
@property (nonatomic, retain) MKPointAnnotation *hitPoint;
@property (nonatomic, retain) MKPolylineView *displayLineView;
@property (nonatomic, retain) CLLocation *currentLocation;
@property CLLocationDirection currentHeading;
@property CLLocationCoordinate2D midpoint;

- (void)initMapView;
- (BOOL)displayHit;
- (void)setupSocket;

@end
