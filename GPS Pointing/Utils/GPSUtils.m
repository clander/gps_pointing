//
//  GPSUtils.m
//  GPS Pointing
//
//  Created by Sven Gehring on 20.09.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "GPSUtils.h"
#include "math.h"

@implementation GPSUtils


#pragma mark - 
#pragma mark Bearing

+ (int)bearing:(CLLocationCoordinate2D)p1 secondPoint:(CLLocationCoordinate2D)p2{
    
    //float dLat = DEGREES_TO_RADIANS(p2.latitude - p1.latitude);
    float dLon = DEGREES_TO_RADIANS(p2.longitude - p1.longitude);
    
    float lat1 = DEGREES_TO_RADIANS(p1.latitude);
    //float long1 = DEGREES_TO_RADIANS(p1.longitude);
    float lat2 = DEGREES_TO_RADIANS(p2.latitude);
    //float long2 = DEGREES_TO_RADIANS(p2.longitude);
    
    float y = sinf(dLon) * cosf(lat2);
    float x = cosf(lat1) * sinf(lat2) - sinf(lat1) * cosf(lat2) * cosf(dLon);
    float brng = RADIANS_TO_DEGREES(atan2f(y, x));
    
    int normalizedBearing = ((int)brng +360) % 360;
    return normalizedBearing;
}


#pragma mark -
#pragma mark Distance

+ (float)equirectangularDistance:(CLLocationCoordinate2D)p1 secondPoint:(CLLocationCoordinate2D)p2{
    float lat1 = DEGREES_TO_RADIANS(p1.latitude);
    float long1 = DEGREES_TO_RADIANS(p1.longitude);
    float lat2 = DEGREES_TO_RADIANS(p2.latitude);
    float long2 = DEGREES_TO_RADIANS(p2.longitude);
    
    float x = (long2-long1) * cosf((lat1+lat2)/2);
    float y = (lat2-lat1);
    float d = sqrtf(x*x + y*y) * EARTH_RADIUS;
    
    return d;
}

+ (float)haversineDistance:(CLLocationCoordinate2D)p1 secondPoint:(CLLocationCoordinate2D)p2{
    
    float dLat = DEGREES_TO_RADIANS(p2.latitude - p1.latitude);
    float dLon = DEGREES_TO_RADIANS(p2.longitude - p1.longitude);
    
    float lat1 = DEGREES_TO_RADIANS(p1.latitude);
    float lat2 = DEGREES_TO_RADIANS(p2.latitude);
    
    float a = sinf(dLat/2.0) * sinf(dLat/2.0) + sinf(dLon/2.0) * sinf(dLon/2.0) * cosf(lat1) * cosf(lat2); 
    float c = 2 * atan2f(sqrtf(a), sqrtf(1-a)); 
    
    float distance = EARTH_RADIUS * c;
    
    return distance;
}

/*+ (float)sphericalDistance:(CLLocationCoordinate2D)p1 secondPoint:(CLLocationCoordinate2D)p2{
    
    // convert coordinates from degrees to radiants
    float lat1 = DEGREES_TO_RADIANS(p1.latitude);
    float long1 = DEGREES_TO_RADIANS(p1.longitude);
    float lat2 = DEGREES_TO_RADIANS(p2.latitude);
    float long2 = DEGREES_TO_RADIANS(p2.longitude);

    float distance = acosf((sinf(lat1) * sinf(lat2)) + cosf(lat1) * cosf(lat2) * cosf(long2 - long1)) * EARTH_RADIUS;
    
    return distance;
}*/

#pragma mark -
#pragma mark Midpoint
+ (CLLocationCoordinate2D)midpoint:(CLLocationCoordinate2D)p1 secondPoint:(CLLocationCoordinate2D)p2{
    double lat1 = DEGREES_TO_RADIANS(p1.latitude);
    double lon1 = DEGREES_TO_RADIANS(p1.longitude);
    double lat2 = DEGREES_TO_RADIANS(p2.latitude);
    //double lon2 = DEGREES_TO_RADIANS(p2.longitude);
    /*double lat1 = p1.latitude;
    double lon1 = p1.longitude;
    double lat2 = p2.latitude;
    double lon2 = p2.longitude;*/
    double dLon = DEGREES_TO_RADIANS(p2.longitude - p1.longitude);
    
    double bx = cos(lat2)*cos(dLon);
    double by = cos(lat2)*sin(dLon);
    
    double lat3 = atan2(sin(lat1)+sin(lat2), sqrt((cos(lat1)+bx)*(cos(lat1)+bx) + by*by));
    double lon3 = lon1 + atan2(by, cos(lat1)+bx);
    
    CLLocationCoordinate2D mp = CLLocationCoordinate2DMake(RADIANS_TO_DEGREES(lat3), RADIANS_TO_DEGREES(lon3));
    
    return mp;
}

#pragma mark -
#pragma mark Intersection 
// Returns the point of intersection of two paths defined by point and bearing
+ (CLLocationCoordinate2D)pathIntersection:(CLLocationCoordinate2D)p1 bearing1:(int)bearingP1 secondPoint:(CLLocationCoordinate2D)p2 bearing2:(int)bearingP2{

    // convert and init all values
    double brng1 = (double)bearingP1;
    double brng2 = (double)bearingP2;
    double lat1 = DEGREES_TO_RADIANS(p1.latitude);
    double lon1 = DEGREES_TO_RADIANS(p1.longitude);
    double lat2 = DEGREES_TO_RADIANS(p2.latitude);
    double lon2 = DEGREES_TO_RADIANS(p2.longitude);
    double brng13 = DEGREES_TO_RADIANS(brng1);
    double brng23 = DEGREES_TO_RADIANS(brng2);
    double dLat = lat2-lat1;
    double dLon = lon2 - lon1;
    
    double dist12 = 2*asin(sqrt(sin(dLat/2)*sin(dLat/2) + cos(lat1)*cos(lat2)*sin(dLon/2)*sin(dLon/2)));
    
    if (dist12==0) {
        return CLLocationCoordinate2DMake(0.0, 0.0);
    }
    
    // initial/final bearings between the points
    double brngA = acos((sin(lat2)-sin(lat1)*cos(dist12)) / (sin(dist12)*cos(lat1)));
    
    if (isnan(brngA)){
        brngA = 0; // protect against rounding
    }
    
    double brngB = acos((sin(lat1)-sin(lat2)*cos(dist12)) / (sin(dist12)*cos(lat2)));
    
    double brng12 = 0.0;
    double brng21 = 0.0;
    
    if (sin(lon2 - lon1) >0) {
        brng12 = brngA;
        brng21 = 2 * M_PI - brngB;
    } else {
        brng12 = 2 * M_PI - brngA;
        brng21 = brngB;
    }
    
    double alpha1 = fmod((brng13 - brng12 + M_PI), (2 * M_PI)) - M_PI; // angle 2-1-3
    double alpha2 = fmod((brng21 - brng23 + M_PI), (2 * M_PI)) - M_PI; // angle 1-2-3
    
    if (sin(alpha1)==0 && sin(alpha2)==0){  // infinite intersections
        return CLLocationCoordinate2DMake(0.0, 0.0);
    }
    if (sin(alpha1)*sin(alpha2 <0)){  // ambiguous intersection
        return CLLocationCoordinate2DMake(0.0, 0.0);
    }
    
    double alpha3 = acos(-cos(alpha1)*cos(alpha2) + sin(alpha1)*sin(alpha2)*cos(dist12));
    double dist13 = atan2(sin(dist12)*sin(alpha1)*sin(alpha2), (cos(alpha2)+cos(alpha1)*cos(alpha3)));
    
    double lat3 = asin(sin(lat1)*cos(dist13) + cos(lat1)*sin(dist13)*cos(brng13));
    
    double dLon13 = atan2(sin(brng13)*sin(dist13)*cos(lat1), cos(dist13)-sin(lat1)*sin(lat3));
    
    double lon3 = lon1 + dLon13;
    lon3 = fmod((lon3 + 3*M_PI),(2*M_PI)) - M_PI; // normalize to -180..180°
    
    return CLLocationCoordinate2DMake(RADIANS_TO_DEGREES(lat3), RADIANS_TO_DEGREES(lon3));
}

@end
