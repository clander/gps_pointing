//
//  GPSUtils.h
//  GPS Pointing
//
//  Created by Sven Gehring on 20.09.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define EARTH_RADIUS 6371 // mean radius in km

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@interface GPSUtils : NSObject

// bearing
+ (int)bearing:(CLLocationCoordinate2D)p1 secondPoint:(CLLocationCoordinate2D)p2;

// distance
+ (float)haversineDistance:(CLLocationCoordinate2D)p1 secondPoint:(CLLocationCoordinate2D)p2;
//+ (float)sphericalDistance:(CLLocationCoordinate2D)p1 secondPoint:(CLLocationCoordinate2D)p2;
+ (float)equirectangularDistance:(CLLocationCoordinate2D)p1 secondPoint:(CLLocationCoordinate2D)p2;

// half-way point between two points
+ (CLLocationCoordinate2D)midpoint:(CLLocationCoordinate2D)p1 secondPoint:(CLLocationCoordinate2D)p2;

// Returns the point of intersection of two paths defined by point and bearing
+ (CLLocationCoordinate2D)pathIntersection:(CLLocationCoordinate2D)p1 bearing1:(int)bearingP1 secondPoint:(CLLocationCoordinate2D)p2 bearing2:(int)bearingP2;

@end
