//
//  MyLocationManager.m
//  GPS Pointing
//
//  Created by Christian Lander on 12.12.12.
//
//

#import "MyLocationManager.h"
#import "GPSUtils.h"
#import "AppDelegate.h"

@implementation MyLocationManager

@synthesize mapDelegate, debugDelegate, currentHeading, currentLocation, motionManager, referenceAttitude, locManager, gyroX, gyroY, gyroZ, leftLat, leftLong, rightLat, rightLong;

static  MyLocationManager* sharedInstance = nil;

// Get the shared instance and create it if necessary.
+ (MyLocationManager *)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

// We can still have a regular init method, that will get called the first time the Singleton is used.
- (id)init
{
    self = [super init];
    
    if (self) {
        AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        leftLat = appDelegate.leftLat;
        leftLong = appDelegate.leftLon;
        rightLat = appDelegate.rightLat;
        rightLong = appDelegate.rightLon;
        
        currentPitch = 0.0;
        
        self.motionManager = [[CMMotionManager alloc] init];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        if (motionManager.gyroAvailable) {
            motionManager.gyroUpdateInterval = 1.0/10.0;
            [motionManager startDeviceMotionUpdatesToQueue:queue withHandler:^(CMDeviceMotion *deviceMotion, NSError *error) {
                
                if (error){
                    // do nothing
                } else {
                    gyroX = RADIANS_TO_DEGREES(deviceMotion.attitude.yaw);
                    gyroY = RADIANS_TO_DEGREES(deviceMotion.attitude.pitch);
                    gyroZ = RADIANS_TO_DEGREES(deviceMotion.attitude.roll);
                    
                    /*
                     [NSString stringWithFormat:@"%.6f", RADIANS_TO_DEGREES(deviceMotion.attitude.yaw)];
                     [NSString stringWithFormat:@"%.6f", RADIANS_TO_DEGREES(deviceMotion.attitude.pitch)];
                     [NSString stringWithFormat:@"%.6f", RADIANS_TO_DEGREES(deviceMotion.attitude.roll)];
                    */
                    
                    currentPitch = deviceMotion.attitude.pitch;
                }
            }];
        }
    }
    
    return self;
}

- (void)dealloc
{
    [motionManager stopDeviceMotionUpdates];
    [locManager stopUpdatingHeading];
    [locManager stopUpdatingLocation];
}

#pragma mark
#pragma mark Core Location Stuff

- (void)startLocationEvents {
    if (!self.locManager) {
        CLLocationManager* theManager = [[CLLocationManager alloc] init];//[[[CLLocationManager alloc] init] autorelease];
        
        // Retain the object in a property.
        self.locManager = theManager;
        locManager.delegate = self;
    }
    
    // Start location services to get the true heading.
    locManager.distanceFilter = 1;
    locManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    [locManager startUpdatingLocation];
    
    // Start heading updates.
    if ([CLLocationManager headingAvailable]) {
        locManager.headingFilter = .1;
        [locManager startUpdatingHeading];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    self.currentLocation = newLocation;
    
    bearingToLeft = [GPSUtils bearing:self.currentLocation.coordinate secondPoint:CLLocationCoordinate2DMake(leftLat, leftLong)];
    bearingToRight = [GPSUtils bearing:self.currentLocation.coordinate secondPoint:CLLocationCoordinate2DMake(rightLat, rightLong)];
    
    if (debugDelegate != NULL) {
        [debugDelegate updateDebug:self.currentLocation bearLeft:bearingToLeft bearRight:bearingToRight pitch:currentPitch];
    }
   // lblBearingLeft.text = [NSString stringWithFormat:@"%i°", bearingToLeft];
   // lblBearingRight.text = [NSString stringWithFormat:@"%i°", bearingToRight];
    
   // [self updateLocationDisplays];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    if (newHeading.headingAccuracy < 0)
        return;
    
    // Use the true heading if it is valid.
    CLLocationDirection  theHeading = ((newHeading.trueHeading > 0) ?
                                       newHeading.trueHeading : newHeading.magneticHeading);
    
    self.currentHeading = theHeading;
    
    if (debugDelegate != NULL) {
        [debugDelegate updateDebug:self.currentHeading];
    }
    //[self updateHeadingDisplays];
    
    // Check if horizontally pointing in display range
    if (mapDelegate != NULL) {
        [mapDelegate updateMap:self.currentLocation bearLeft:bearingToLeft bearRight:bearingToRight heading:self.currentHeading pitch:currentPitch];
    }
    //[self displayHit];
    //NSLog(@"Horizontally pointing to display: %i", [self displayHit]);
    
    // Rotate the map according to the compass orientation
    //[mapView setTransform:CGAffineTransformMakeRotation(-1 * theHeading * 3.14159 / 180)];
}

@end
