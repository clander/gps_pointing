//
//  Communicator.m
//  GPS Pointing
//
//  Created by Christian Lander on 17.01.13.
//
//

#import "Communicator.h"
#import "Globals.h"
#import "AppDelegate.h"
#import <ifaddrs.h>
#import <arpa/inet.h>
#import "MyLocationManager.h"

@implementation Communicator

@synthesize udpSocket, ipAdress, imgDelegate;

static Communicator *sharedInstance = nil;

// Get the shared instance and create it if necessary.
+ (Communicator *)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

// We can still have a regular init method, that will get called the first time the Singleton is used.
- (id)init
{
    self = [super init];
    
    if (self) {
        if (udpSocket == nil)
        {
            [self setupSocket];
        }
    }
    
    return self;
}

- (void)setupSocket
{
    NSError *error = nil;
    udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
	/*
    if (![udpSocket connectToHost:self.ipAdress onPort:PORT error:&error])
    {
        NSLog(@"%@", error);
		return;
    }
	*/
    
	if (![udpSocket bindToPort:PORT error:&error])
	{
        NSLog(@"%@", error);
		return;
	}
     
	if (![udpSocket beginReceiving:&error])
	{
        NSLog(@"%@", error);
		return;
	}
}

-(void) sendConnect
{
    
    NSString *msg = [NSString stringWithFormat:@"CONNECT#%@", [self getIPAddress]];
	if ([msg length] == 0)
	{
		return;
	}
	
	NSData *data = [msg dataUsingEncoding:NSUTF8StringEncoding];
	[udpSocket sendData:data toHost:self.ipAdress port:PORT withTimeout:-1 tag:_tag];
    
    NSLog(@"sent connect message");
    
    _tag++;
}

-(void) closeConnection
{
    [udpSocket close];
}

- (NSString *)getIPAddress
{
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *wifiAddress = nil;
    NSString *cellAddress = nil;
    
    // retrieve the current interfaces - returns 0 on success
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if(sa_type == AF_INET || sa_type == AF_INET6) {
                NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
                NSString *addr = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)]; // pdp_ip0
                //NSLog(@"NAME: \"%@\" addr: %@", name, addr); // see for yourself
                
                if([name isEqualToString:@"en0"]) {
                    // Interface is the wifi connection on the iPhone
                    wifiAddress = addr;
                } else
                    if([name isEqualToString:@"pdp_ip0"]) {
                        // Interface is the cell connection on the iPhone
                        cellAddress = addr;
                    }
            }
            temp_addr = temp_addr->ifa_next;
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    NSString *addr = wifiAddress ? wifiAddress : cellAddress;
    return addr ? addr : @"0.0.0.0";
}

-(void) sendUpdate:(float)x andY:(float)y
{
    NSString *msg = [NSString stringWithFormat:@"POINTING#%f#%f", x, y];
	if ([msg length] == 0)
	{
		return;
	}
	
	NSData *data = [msg dataUsingEncoding:NSUTF8StringEncoding];
	[udpSocket sendData:data toHost:self.ipAdress port:PORT withTimeout:-1 tag:_tag];
    
    NSLog(@"sent pointing message");
    
    _tag++;
}


- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
	// You could add checks here
    NSLog(@"sent data with tag %ld", tag);
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
	// You could add checks here
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext
{
	NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSLog(@"received%@", msg);
    
    NSArray* splittedMsg = nil;
    if (msg != nil) {
        splittedMsg = [[NSArray alloc] initWithArray:[msg componentsSeparatedByString:@"#"]];
    }
    
    if (splittedMsg != nil && splittedMsg.count > 0 && [[splittedMsg objectAtIndex:0] isEqualToString:@"GPS"]) {
        AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        appDelegate.leftLat = [[splittedMsg objectAtIndex:1] floatValue];
        appDelegate.leftLon = [[splittedMsg objectAtIndex:2] floatValue];
        appDelegate.rightLat = [[splittedMsg objectAtIndex:3] floatValue];
        appDelegate.rightLon = [[splittedMsg objectAtIndex:4] floatValue];
        appDelegate.lower = [[splittedMsg objectAtIndex:5] floatValue];
        appDelegate.upper = [[splittedMsg objectAtIndex:6] floatValue];
        
        MyLocationManager* ml = [MyLocationManager sharedInstance];
        
        [ml setLeftLat:appDelegate.leftLat];
        [ml setLeftLong:appDelegate.leftLon];
        [ml setRightLat:appDelegate.rightLat];
        [ml setRightLong:appDelegate.rightLon];
        
        [ml startLocationEvents];
        NSLog(@"started location manager...");
    } else {
        UIImage* img = [[UIImage alloc] initWithData:data];
        if (img != nil) {
            NSLog(@"updates image...");
            [imgDelegate updateImage:img];
        }
    }
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didConnectToAddress:(NSData *)address
{
    NSLog(@"connected to %@", address);
}

@end
