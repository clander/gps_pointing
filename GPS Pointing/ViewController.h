//
//  ViewController.h
//  GPS Pointing
//
//  Created by Sven Gehring on 16.09.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyLocationManager.h"
#import "AppDelegate.h"
/*
#define kLatLeft        @"leftlat"
#define kLongLeft       @"leftlong"
#define kLatRight       @"rightlat"
#define kLongRight      @"rightlong"
*/
@interface ViewController : UIViewController<DebugDelegate, UITextFieldDelegate>{
    
    IBOutlet UILabel *lblHeading;
    IBOutlet UILabel *lblLatitude;
    IBOutlet UILabel *lblLongitude;
    IBOutlet UILabel *lblAccuracy;
    IBOutlet UILabel *lblVerticalAccuracy;
    IBOutlet UILabel *lblAltitude;
    IBOutlet UILabel *lblGyroX;
    IBOutlet UILabel *lblGyroY;
    IBOutlet UILabel *lblGyroZ;
    IBOutlet UILabel *lblBearingLeft;
    IBOutlet UILabel *lblBearingRight;
   // IBOutlet MKMapView *mapView;
    IBOutlet UISwitch *gpsSwitch;
    
    //CLLocationManager *locManager;
    CLLocationDirection currentHeading;
    CLLocation *currentLocation;
    //CLLocationCoordinate2D midpoint;
    
    //CMMotionManager * motionManager;
    //CMAttitude *referenceAttitude;
    
    //BOOL drawHitOverlay;
    /*
    float leftLat;
    float leftLong;
    float rightLat;
    float rightLong;
    MKPolyline *displayLine;
    MKPolylineView *displayLineView;
    MKPointAnnotation *hitPoint;
    
    int bearingToLeft;
    int bearingToRight;
    
    double deltaHeight; // height difference between phone and lower display boundary
    double currentPitch; // in RADIANS
    
    double displayHeight;*/
}

@property (nonatomic, retain) IBOutlet UITextField *ipAdress;
@property (nonatomic, retain) IBOutlet UILabel *lblHeading;
@property (nonatomic, retain) IBOutlet UILabel *lblLatitude;
@property (nonatomic, retain) IBOutlet UILabel *lblLongitude;
@property (nonatomic, retain) IBOutlet UILabel *lblAccuracy;
@property (nonatomic, retain) IBOutlet UILabel *lblVerticalAccuracy;
@property (nonatomic, retain) IBOutlet UILabel *lblAltitude;
@property (nonatomic, retain) IBOutlet UILabel *lblGyroX;
@property (nonatomic, retain) IBOutlet UILabel *lblGyroY;
@property (nonatomic, retain) IBOutlet UILabel *lblGyroZ;
@property (nonatomic, retain) IBOutlet UILabel *lblBearingLeft;
@property (nonatomic, retain) IBOutlet UILabel *lblBearingRight;
//@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic, retain) IBOutlet UISwitch *gpsSwitch;
//@property (nonatomic, retain) MKPolyline *displayLine;
//@property (nonatomic, retain) MKPolylineView *displayLineView;
//@property (nonatomic, retain) MKPointAnnotation *hitPoint;
//@property (nonatomic, retain) CLLocationManager *locManager;
@property (nonatomic, retain) CLLocation *currentLocation;
//@property (nonatomic, retain) CMMotionManager *motionManager;
//@property (nonatomic, retain) CMAttitude *referenceAttitude;
@property CLLocationDirection currentHeading;
//@property CLLocationCoordinate2D midpoint;


//- (void)initMapView;
- (void)updateHeadingDisplays;
- (void)updateLocationDisplays;
- (void)updateFieldsFromSettings;
//- (void)mapView:(MKMapView *)mv regionWillChangeAnimated:(BOOL)animated;

- (IBAction)disconnect:(id)sender;
- (IBAction)connect:(id)sender;

// map 
//- (BOOL)displayHit;
//- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay;

// location
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation;
//- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading;


@end