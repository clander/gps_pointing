//
//  MyLocationManager.h
//  GPS Pointing
//
//  Created by Christian Lander on 12.12.12.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>

@protocol MapDelegate <NSObject>

-(void) updateMap:(CLLocation*) curLocation bearLeft:(int) bl bearRight:(int) br heading:(CLLocationDirection) curHeading pitch:(double)cp;

@end

@protocol DebugDelegate <NSObject>

-(void) updateDebug:(CLLocation*) curLocation bearLeft:(int) bl bearRight:(int) br pitch:(double)cp;
-(void) updateDebug:(CLLocationDirection)curHeading;

@end

@interface MyLocationManager : NSObject <CLLocationManagerDelegate>  {
    
    CLLocationManager *locManager;
    CLLocation *currentLocation;
    CLLocationCoordinate2D midpoint;
    CLLocationDirection currentHeading;
    
    CMMotionManager * motionManager;
    CMAttitude *referenceAttitude;
    
    int bearingToLeft;
    int bearingToRight;
    
    double currentPitch;
    
    float leftLat;
    float leftLong;
    float rightLat;
    float rightLong;
    
    float gyroX;
    float gyroY;
    float gyroZ;
}

@property (nonatomic, retain) CLLocationManager *locManager;
@property (nonatomic, retain) CLLocation *currentLocation;
@property CLLocationDirection currentHeading;
@property (nonatomic, retain) id<MapDelegate> mapDelegate;
@property (nonatomic, retain) id<DebugDelegate> debugDelegate;
@property (nonatomic, retain) CMMotionManager *motionManager;
@property (nonatomic, retain) CMAttitude *referenceAttitude;
@property float gyroX;
@property float gyroY;
@property float gyroZ;
@property float leftLat;
@property float leftLong;
@property float rightLat;
@property float rightLong;

+ (id)sharedInstance;
- (void)startLocationEvents;

@end
