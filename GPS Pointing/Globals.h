//
//  Globals.h
//  GPS Pointing
//
//  Created by Christian Lander on 12.12.12.
//
//

#import <Foundation/Foundation.h>

@interface Globals : NSObject

extern  NSString* const DISPLAY_LEFT_LONG;
extern  NSString* const DISPLAY_LEFT_LAT;
extern  NSString* const DISPLAY_RIGHT_LONG;
extern  NSString* const DISPLAY_RIGHT_LAT;

extern NSString* const DISPLAY_LOWER_BOUND;
extern NSString* const DISPLAY_UPPER_BOUND;

extern int const PORT;

@end
