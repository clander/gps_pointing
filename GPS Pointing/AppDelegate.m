//
//  AppDelegate.m
//  GPS Pointing
//
//  Created by Sven Gehring on 16.09.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize leftLat, leftLon, rightLat, rightLon, lower, upper;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //home display
    //49.346385,7.00513 - right
    //49.346494,7.00546 - left
    
    // Get user preference
    defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *leftLonDefault = [NSDictionary dictionaryWithObject:@"7.00513" forKey:DISPLAY_LEFT_LONG];
    [defaults registerDefaults:leftLonDefault];
    NSDictionary *leftLatDefault = [NSDictionary dictionaryWithObject:@"49.346385" forKey:DISPLAY_LEFT_LAT];
    [defaults registerDefaults:leftLatDefault];
    NSDictionary *rightLonDefault = [NSDictionary dictionaryWithObject:@"7.00546" forKey:DISPLAY_RIGHT_LONG];
    [defaults registerDefaults:rightLonDefault];
    NSDictionary *rightLatDefault = [NSDictionary dictionaryWithObject:@"49.346494" forKey:DISPLAY_RIGHT_LAT];
    [defaults registerDefaults:rightLatDefault];
    NSDictionary *lowerDefault = [NSDictionary dictionaryWithObject:@"5" forKey:DISPLAY_LOWER_BOUND];
    [defaults registerDefaults:lowerDefault];
    NSDictionary *upperDefault = [NSDictionary dictionaryWithObject:@"10" forKey:DISPLAY_UPPER_BOUND];
    [defaults registerDefaults:upperDefault];
    [defaults synchronize];
    
    NSString *string = [defaults stringForKey:DISPLAY_LEFT_LAT];
    leftLat = [string floatValue];
    
    string = [defaults stringForKey:DISPLAY_LEFT_LONG];
    leftLon = [string floatValue];
    
    string = [defaults stringForKey:DISPLAY_RIGHT_LAT];
    rightLat = [string floatValue];
    
    string = [defaults stringForKey:DISPLAY_RIGHT_LONG];
    rightLon = [string floatValue];
    
    string = [defaults stringForKey:DISPLAY_LOWER_BOUND];
    lower = [string floatValue];
    
    string = [defaults stringForKey:DISPLAY_UPPER_BOUND];
    upper = [string floatValue];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

@end
